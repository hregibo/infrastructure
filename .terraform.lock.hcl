# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/danitso/proxmox" {
  version     = "0.4.4"
  constraints = "0.4.4"
  hashes = [
    "h1:y55KDEXDpx0iQrfa5ceCsY1FEW07AK0Rrt/c1sMdXfQ=",
    "zh:0dfd6e96fb4428bbd77f9cdecbdcdc518c6a3a2a75d98485b90c2f099b6c3566",
    "zh:2e333be6637730a1e641f3b47a230487a6d6b5cd0ddecf9ef20b6e12f03f6090",
    "zh:67be876dc1926c1986cbda3b4cce9be82beb7996c2756c32d34078b57431f118",
    "zh:84c0101cc0828ac438aa33a0d3fab8a40903cee803d0bbfed2137e6ea0da0fec",
    "zh:8508ac691e1ccb7e7b4e9bf8d6bcf8a156d139f8dcd91df0fce2584ab9f76937",
    "zh:9f37e4ce4bc887b8838e8181d73f2cfea1410cd772fdfa3755c5fd6f87fd394e",
    "zh:a3f7ea5bc0b9ee9c97b40c92913611f79039c7de679991d5f3e5a5844f637199",
    "zh:b25fab3b993cbc6bc670bb4611dc6a9b08a730e845ba743cc796d1fcd03ef9ba",
    "zh:bde3435e887a036805678249c6c62b1c7976b11d9232491b00ad5f60dc638682",
    "zh:c6521be31903214c7775e0d68bb8169e42478ed3c209b7d1942bfecd2c3df9e6",
    "zh:e5996a6f1ad5354021ba25059f4cd9097c2d348556a07f4220e9d2e4d0a76881",
  ]
}

provider "registry.terraform.io/hashicorp/dns" {
  version = "3.2.3"
  hashes = [
    "h1:ODcR+vWOhCAJ2iCChZMVdRglNCx07VNr67OPLRPZyDY=",
    "zh:03a304f4b76ac6c8bebffddcdf555bf77578a7f638948a681589def32e140cb8",
    "zh:08c7d2498b747054e9c9df7838bfa4e4a6b5d63e2d29f0457247e384f792d56c",
    "zh:20adf489819ba51ba9d9d15da2dbe1fecb92491b3d0dd80096873e5e84d8b4bd",
    "zh:2959ff209d2578456ca490672b82864d483b9e9db9efc8e4ffada06e23017609",
    "zh:3ecd0b22db79550fb1108ff7bd00c4066825e8c23bb64e3cc8d9b8102e8caa45",
    "zh:6e53a9232245b4be52b56b078f15f270b89afe6abb9c9b8baab4a282fe0cf9f8",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:80437bdfa08eb90f70105b52cb06799a8f7967313654b43d28d7f654fcd4edc1",
    "zh:816ddaca0ecc29e287376e5b0b8b0729ee13f23a9d74bfad5b14b7983e1a1775",
    "zh:82d8ac7ad00c1a71d0a7c1aca03bb59a6b51128f895242df80b1f3d016c3c51a",
    "zh:ec9243b8bd80693a6eeeea5d4f7f4e6f57bd44ae796d6d5b1a91790e359f8a61",
    "zh:fd821adbfb03a2c9eac111ff27a32b3a5523b18f80333008de85482d3bbea645",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version = "3.1.2"
  hashes = [
    "h1:5A5VsY5wNmOZlupUcLnIoziMPn8htSZBXbP3lI7lBEM=",
    "zh:0daceba867b330d3f8e2c5dc895c4291845a78f31955ce1b91ab2c4d1cd1c10b",
    "zh:104050099efd30a630741f788f9576b19998e7a09347decbec3da0b21d64ba2d",
    "zh:173f4ef3fdf0c7e2564a3db0fac560e9f5afdf6afd0b75d6646af6576b122b16",
    "zh:41d50f975e535f968b3f37170fb07937c15b76d85ba947d0ce5e5ff9530eda65",
    "zh:51a5038867e5e60757ed7f513dd6a973068241190d158a81d1b69296efb9cb8d",
    "zh:6432a568e97a5a36cc8aebca5a7e9c879a55d3bc71d0da1ab849ad905f41c0be",
    "zh:6bac6501394b87138a5e17c9f3a41e46ff7833ad0ba2a96197bb7787e95b641c",
    "zh:6c0a7f5faacda644b022e7718e53f5868187435be6d000786d1ca05aa6683a25",
    "zh:74c89de3fa6ef3027efe08f8473c2baeb41b4c6cee250ba7aeb5b64e8c79800d",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:b29eabbf0a5298f0e95a1df214c7cfe06ea9bcf362c63b3ad2f72d85da7d4685",
    "zh:e891458c7a61e5b964e09616f1a4f87d0471feae1ec04cc51776e7dec1a3abce",
  ]
}

provider "registry.terraform.io/ovh/ovh" {
  version     = "0.17.1"
  constraints = "0.17.1"
  hashes = [
    "h1:6CHP8imjJoG1mAxwLqo1DNBmNptkbKl6IjUAdNxDLA4=",
    "h1:AQHMomRbpX/laaJFvlcpzygMjLStTGeYKlUyGJYPym0=",
    "zh:037db4b87687eeaae027e65be10b8f58078b06656d492053a43acae55c7da947",
    "zh:047abb6b79761d0985744070d57a70fe1ee697b942878f87fd3eaa31bb70dd77",
    "zh:0564f1059844d40ce553554877b8948db9f495489a7e768b5f568f76c9a627c8",
    "zh:0acc54d224fddbe02357b9e429e730d7deccdc3db3ab59db8eae437894310714",
    "zh:2286a5cd52d25ce61a6ad8e67d1301c93ca1042872354a5d72139d61dcfaeb76",
    "zh:351f0e1fb7b094b316b739cd25f9abf788067843fafb57e05e6d2cc5f2743088",
    "zh:641cc5225ef1d57bd173fe7dd5a9ce6aca2d684982536c2b456410c3e5b33789",
    "zh:6c47ff1c508e092ec0fc98e623f8f3692fb6deec401593784100d0db70f6586a",
    "zh:7f8340a8faf702ac495e8cb0c621e5a82a3909ffb8fbbae10dcce1ebcc29c40b",
    "zh:b548ed9d7ffa93f69cf478053325fe3e4f122e056e93228e1d3da3b00cf1cbcc",
    "zh:bc00a01446fd9de7713ce1d878f17f45ee7ae8b4d870c1648bd5db29b15e7949",
    "zh:d7d8cba424edc461be87b485d8f813e59603f55911b14994862dd15e9a53fcff",
    "zh:f84087f554ab56a064e5bee9895d2c8128621f8445a5607cb807ebe5c732e876",
  ]
}
