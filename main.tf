resource ovh_me_ssh_key "hugo_xps13" {
  key_name = "hugo_xps13"
  key      = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFIDuTRO8iQtCvzCP2Reum3x6zFfOmd4s+3zHQzXQYTF hugoregibo@XPS-03"
}

resource ovh_me_ssh_key "hugo_desktop" {
  key_name = "hugo_desktop"
  key      = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGoSKRfMXCDxD3cNc78Jc4261FagKojrDrT9Enh7JnlO hugo@weasel"
  default  = true
}
