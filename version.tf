terraform {
  backend "local" {}

  required_version = "~> 1.1.7"
  required_providers {
    ovh = {
      source  = "ovh/ovh"
      version = "0.17.1"
    }
    proxmox = {
      source  = "danitso/proxmox"
      version = "0.4.4"
    }
  }
}

provider "ovh" {
  endpoint           = "ovh-eu"
  application_key    = var.ovh_app_key
  application_secret = var.ovh_app_secret
  consumer_key       = var.ovh_consumer_key
}

provider "proxmox" {
  virtual_environment {
    endpoint = var.proxmox_mink_host
    username = var.proxmox_mink_user
    password = var.proxmox_mink_pass
    insecure = true
  }
}
