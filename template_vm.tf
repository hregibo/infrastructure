# resource proxmox_virtual_environment_vm "template_deb11" {
#   name        = "tpl-deb11"
#   description = "Managed by Terraform"

#   node_name = "mink"
#   on_boot   = true
#   vm_id     = 990

#   agent {
#     enabled = true
#   }

#   disk {
#     datastore_id = "local-lvm"
#     interface    = "scsi0"
#     size         = 16
#   }

#   memory {
#     dedicated = 2048
#   }

#   initialization {
#     ip_config {
#       ipv4 {
#         address = ""
#         gateway = ""
#       }
#       ipv6 {
#         address = ""
#         gateway = ""
#       }
#     }

#     user_account {
#       keys = [
#         resource.ovh_me_ssh_key.hugo_desktop.key,
#         resource.ovh_me_ssh_key.hugo_xps13.key
#       ]
#       username = "root"
#     }
#   }

#   network_device {
#     mac_address = ""
#   }

#   operating_system {
#     type = "l26"
#   }

#   depends_on = [
#     data.ovh_dedicated_server.mink
#   ]
# }

# resource proxmox_virtual_environment_file "debian_cloud_image" {
#   content_type = "iso"
#   datastore_id = "local"
#   node_name    = "mink"

#   source_file {
#     path = "https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-genericcloud-amd64.qcow2"
#   }

#   depends_on = [
#     data.ovh_dedicated_server.mink
#   ]
# }
