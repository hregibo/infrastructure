variable "account" {
  type        = string
  description = "the account name for the infrastructure admin"
  default     = "root"
}

variable "ovh_app_key" {
  sensitive = true
  type      = string
}

variable "ovh_app_secret" {
  sensitive = true
  type      = string
}

variable "ovh_consumer_key" {
  sensitive = true
  type      = string
}

variable "private_key_path" {
  type    = string
  default = "~/.ssh/id_ed25519"
}

variable "proxmox_mink_host" {
  type        = string
  description = "The Proxmox endpoint to contact"
}
variable "proxmox_mink_user" {
  type        = string
  description = "The Proxmox user for server mink"
  default     = "root@pam"
}
variable "proxmox_mink_pass" {
  type        = string
  sensitive   = true
  description = "The Proxmox password for server Mink"
}
