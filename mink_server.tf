data ovh_dedicated_server "mink" {
  service_name = "ns376016.ip-5-135-141.eu"
}

data dns_a_record_set "mink" {
  host = data.ovh_dedicated_server.mink.name
}

data ovh_ip_service "mink_v4_s29_01" {
  service_name = "ip-5.196.105.240/29"
}

data ovh_dedicated_server_boots "mink" {
  service_name = data.ovh_dedicated_server.mink.name
  boot_type    = "rescue"
}

resource random_password "password" {
  length           = 32
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

locals {
  private_key_content = file(var.private_key_path)
}

resource ovh_dedicated_server_install_task "mink" {
  service_name      = data.ovh_dedicated_server.mink.name
  template_name     = "proxmox7_64"
  bootid_on_destroy = data.ovh_dedicated_server_boots.mink.result[0]
  details {
    custom_hostname = "mink"
    ssh_key_name    = resource.ovh_me_ssh_key.hugo_desktop.key_name
  }

  connection {
    type        = "ssh"
    user        = "root"
    private_key = local.private_key_content
    host        = data.dns_a_record_set.mink.addrs[0]
  }

  provisioner "remote-exec" {
    inline = [
      "echo 'root:${var.proxmox_mink_pass}' | chpasswd"
    ]
  }
}

resource ovh_domain_zone_record "mink" {
  zone      = data.ovh_domain_zone.lumikko.name
  subdomain = "mink"
  fieldtype = "A"
  ttl       = "3600"
  target    = data.ovh_dedicated_server.mink.ip
}
