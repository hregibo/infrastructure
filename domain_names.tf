
data ovh_domain_zone "lumikko" {
  name = "lumikko.dev"
}

data ovh_domain_zone "d3v" {
  name = "d3v.fi"
}

data ovh_domain_zone "mihoka" {
  name = "mihoka.me"
}

data ovh_domain_zone "eve2twitch" {
  name = "eve2twitch.space"
}

data ovh_domain_zone "hugoregibo" {
  name = "hugoregibo.be"
}

data ovh_domain_zone "elkinoo" {
  name = "elkinoo.tv"
}
